package com.example.language;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.example.language.databinding.ActivityMapsBinding;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ActivityMapsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMapsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng MANCHESTER = new LatLng(55.99914872389745, 92.79873518813244);
        LatLng LoveEnglishClub = new LatLng(56.01047434851533, 92.79238371770104);
        LatLng Okey = new LatLng(56.013849063162915, 92.80941993861661);
        LatLng Enjoy = new LatLng(56.01711147766775, 92.81628639313703);
        LatLng BrightClub = new LatLng(56.01456873711011, 92.82993347149639);


        mMap.addMarker(new MarkerOptions().position(MANCHESTER).title("MANCHESTER"));
        mMap.addMarker(new MarkerOptions().position(LoveEnglishClub).title("Love English Club"));
        mMap.addMarker(new MarkerOptions().position(Okey).title("O`key"));
        mMap.addMarker(new MarkerOptions().position(Enjoy).title("Enjoy"));
        mMap.addMarker(new MarkerOptions().position(BrightClub).title("Bright Club"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(BrightClub));
    }
}