package com.example.language;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AlphabetActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alphabet);
    }

    public void onButtonClick(View V){
        Intent intent = new Intent(AlphabetActivity.this, MainActivity.class);
        startActivity(intent);
    }

}