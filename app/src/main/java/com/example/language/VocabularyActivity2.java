package com.example.language;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class VocabularyActivity2 extends AppCompatActivity {

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary2);

        dbHelper = new DBHelper(this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        TextView text =(TextView) findViewById(R.id.textView3);

        Cursor cursor = database.query(DBHelper.TABLE_CONTACTS, null,
                null, null, null, null, null);

        if(cursor.moveToFirst()){
            int idIndex = cursor.getColumnIndex(DBHelper.KEY_ID);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_USERWORD);

            do {
                int id =  cursor.getInt(idIndex);
                text.append("\n" + cursor.getString(nameIndex)  );
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    public void onButtonClick(View V){
        Intent intent = new Intent(VocabularyActivity2.this, MainActivity.class);
        startActivity(intent);
    }

    public void onButtonClick1(View V){
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        database.delete(DBHelper.TABLE_CONTACTS,null,null);
        VocabularyActivity2.this.recreate();
    }

    public void onButtonClick2(View V){
        final EditText word = (EditText)findViewById(R.id.editTextTextPersonName2);

        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(dbHelper.KEY_USERWORD, word.getText().toString());
        database.insert(dbHelper.TABLE_CONTACTS,null , contentValues);
        VocabularyActivity2.this.recreate();

    }

}